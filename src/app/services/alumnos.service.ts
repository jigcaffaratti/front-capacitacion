import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../app.config';
import { map } from 'rxjs/operators';
import { Alumno } from '../models/alumno';
import { Observable } from 'rxjs';
@Injectable()
export class AlumnosService {
  constructor(private http: HttpClient, private config: AppConfig) {}

  obtenerAlumnos(): Observable<Alumno[]> {
    return this.http.get<Alumno[]>(`${this.config.apiUrl}/alumnos`).pipe(
      map((response) => {
        return response;
      })
    );
  }
}
